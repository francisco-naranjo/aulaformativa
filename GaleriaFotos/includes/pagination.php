 <?php
	require_once(LIB_DIR . SD . "database.php");

	class pagination
	{	// clase encargada de la paginación de registros de la base de datos
		public $page_index;
		public $page_records;
		public $total_records;
		public $page_min;/**/
		public $page_max;/**/

		protected static $table_name;

		function __construct($page_index = 1, $page_records = 10, $total_records = 0) {
			$this->page_index = $page_index; 
			$this->page_records = $page_records; 
			$this->total_records = $total_records; 
		}

		public function page_total() {
			// Devolver el entero mas próximo superior
			$pages = ceil($this->total_records / $this->page_records);
			return $pages;
		}

		public function page_next() {
			return $this->page_index + 1;
		}

		public function page_preview() {
			return $this->page_index - 1;
		}

		public function page_next_exist() {
			return ($this->page_index < $this->page_total());
		}

		public function page_preview_exist() {
			return ($this->page_index > 1 );
		}
		
		public function offset() {
			return ($this->page_index - 1) * $this->page_records;
		}
	}
?>
