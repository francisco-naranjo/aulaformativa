<?php 
	/*
	** Validar los campos del 2do parametro, de acuerdo a las expresiones regulares del 1er parametro
	** @param array $validations
	** @param array $fields
	** @return boll o array
	*/
	function validate($validations, $fields) {
		$errors = array("total_errors" => 0); /* Contabilizar errores */
		foreach ($validations as $field => $regular_expresion) {
			if (!preg_match($regular_expresion, $fields[$field])) {
				$errors["total_errors"]++;
				$errors[$fields] = true;
			}
		}
		if ($errors["total_errors"] == 0) {
			return false;
		}
		else {
			return $errors;
		}
	}

	/* La expresión regular puede contener de 1 a 5 caracteres alfanumericos, espacios y signos de puntuación */
	$form_validations = array("Title" => "/^[[:alnum:][:space:][:punct:]]{1,5}$/");

	$form_fields = array("Title" => "Este es un texto de prueba");

	print_r (validate($form_validations, $form_fields));
?> 