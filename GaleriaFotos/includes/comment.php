 <?php require_once(LIB_DIR . SD . "database.php");

	class comment extends table
	{	// Clase photo que que se extiende de la clase base table
		public $Id;
		public $foto_id;
		public $creado;
		public $autor;
		public $contenido;

		protected static $table_name = "comentarios";
		protected static $table_fields = array("foto_id", "creado", "autor", "contenido");

		public static function create_comment($foto_id, $autor, $contenido) {	// Método que permite crear un nuevo comentario // crear_nuevo()
			echo "<script language = JavaScript> alert ('Clase comment \\n método create_comment() \\n \$foto_id = {$foto_id} \\n {$autor} \\n {$contenido}')</script>";
			echo 'Clase comment, método create_comment(), \$foto_id = {$foto_id}, {$autor}, {$contenido}';
			if (!empty($foto_id) && !empty($autor) && !empty($contenido)) {
				$comment = new comment();
				$comment->foto_id = (int)$foto_id;
				$comment->autor = $autor;
				$comment->contenido = $contenido;
				$comment->creado = strftime("%Y-%m-%d %H:%M:%S", time());
				return $comment;
			}
		} 	// Fin del método create_comment()

		public function photo_comment($foto_id) {	// Método que permite buscar los comntarios relativos a una foto // comentarios_por_id()
			global $db_obj;
			$query = "SELECT * FROM " . self::$table_name;
			$query .= " WHERE foto_id = " . $db_obj->query_prepary($foto_id); // preparar_consulta()
			$query .= " ORDER BY creado ASC;";
			return self::find_query($query); // buscar_por_sql()
		} 	// Fin del método photo_comment comentarios_por_id()
	} 	// Fin de la clase photo()
?>