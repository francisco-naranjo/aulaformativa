 <?php 
	require_once(LIB_DIR . SD . "config.php");
 	
	class user extends table
	{	// Clase usuario que se extiende de la clase base tabla
		public $Id;
		public $usuario;
		public $clave;
		public $nombre;
		public $apellido;

		protected static $table_name = "usuarios";
		protected static $table_fields = array("usuario", "clave", "nombre", "apellido");

		public function full_name()
		{	// Método que retorna el nombre completo del usuario
			if (isset($this->nombre) && isset($this->apellido)) 
			{
				return $this->nombre . " " . $this->apellido . "<br>";
			}
			else
			{
				return "";
			}
		} 	// Fin del método users_find()

		public function username_find($username)
		{	// Método que recibe un username y retona el registro encontrado en la base de datos
			global $db_obj;
			$query = "SELECT Id FROM " . static::$table_name . " WHERE usuario = '{$username}';";
			/*Invocar el método find_query() de la clase table que ejecuta la instrucción SQL en la BD */
			//$users_array = array();
			$array = static::find_query($query);
			return (!empty($array)) ? array_shift($array) : false;
		} 	// Fin del método username_find()

		public static function authenticate_user($username="", $password="")
		{	// Método que valida el username y la clave de un usuario
			global $db_obj; // Traer el objeto $db_obj al método
			/*echo "<script language = JavaScript> alert (' 1 Clase user \\n Método authenticate_user(\$usuario = {$username}, clave = {$password}) \\n y pasa al método query_prepary()')</script>";*/
			$username = $db_obj->query_prepary($username);
			$password = $db_obj->query_prepary($password);
			// Crear un alert para verificar la infomación antes de redirigir al usuario
			/*echo "<script language = JavaScript> alert (' 2 Clase user \\n Método authenticate_user() \\n RETORNOS DE query_prepary \\n \$usuario = {$username} \\n \$clave = {$password}')</script>";*/
			$query = "SELECT * FROM usuarios WHERE usuario = '{$username}' AND clave = '{$password}';";
			$users_array = self::find_query($query);
			return (!empty($users_array)) ? array_shift($users_array) : false;
		} 	// Fin del método authenticate_user()

	} 	// Fin de la clase user()
?>