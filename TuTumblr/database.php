<?php 
	/* ABRIR UNA CONEXION CON EL SERVIDOR MySql
	** @return resource "RETORNA LA CONEXION"
	*/
	function connect_db_open() {
		// Conectar con el servidor de Bases de Datos
		$connection = mysql_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);
		if (!$connection) {
			return false;
		}
		else {
			$message = "Se conecto el usuario <strong style=\"color:#F00;\">" . DB_USERNAME . "</strong> con el servidor de Bases de Datos <strong style=\"color:#F00;\">" . DB_SERVER . "</strong><br>";
			//echo $message;
		}
		// Seleccionar la Base de Datos
		$db_select = mysql_select_db(DB_NAME, $connection);
		if (!$db_select) {
			$message = "Al seleccionar la base de datos <strong style=\"color:#F00;\">" . DB_NAME . "</strong> se presenta el siguiente ERROR: ";
			die($message . mysql_error()) . "<br>";
		} 
		else {
			$message = "Se selecciono con exito la base de datos <strong style=\"color:#F00;\">" . DB_NAME . "</strong><br>";
			//echo $message;
		}
		// Retornar la conexión
		return $connection;
	}// Fin del método connect_db_open()

	/* CERRAR UNA CONEXION CON EL SERVIDOR MySql */
	function connect_db_close() {
		if (isset($connection)) {
			mysql_close($connection);
			unset($connection);
		}
	}// Fin del método connect_close()

	/* CONVIERTE UN RECURSO A MATRIZ
	** @param resource $resource
	** @return array
	*/
	function resource_array($resource) {
		$array = array();
		while ($row = mysql_fetch_array($resource)) {
			array_push($array, $row);
		}
			return $array;
	}// Fin del método resource_array()
?>