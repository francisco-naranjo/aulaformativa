 <?php 
	require_once(LIB_DIR . SD . "database.php");

	class table
	{	// Clase base que contiene los métodos comunes entre varias tablas de la base de datos
		protected static $table_name;
		protected static $table_fields;

		public static function find_id($Id)
		{	// Método que recibe un Id y retorna el registro encontrado en la base de datos
			global $db_obj;
			/*echo "<script language = JavaScript> alert (' 1 Clase table \\n Metodo find_id() \\n Id = {$Id}')</script>";*/
			$query = "SELECT * FROM " . static::$table_name . " WHERE Id = " . $db_obj->query_prepary($Id) .";";
			/*Invocar el método find_query() de la clase table que ejecuta la instrucción SQL en la BD */
			$array = static::find_query($query);
			return (!empty($array)) ? array_shift($array) : false;
		} 	// Fin del método find_id()

		public static function find_all() // buscar_todos()
		{	// Método que retorna todos los registros de una tabla de la base de datos
			$query = "SELECT * FROM  " . static::$table_name . ";";
			/*Invocar el método find_query() de la clase table que ejecuta la instrucción SQL en la BD */
			$query_result = static::find_query($query);
			return $query_result;
		} 	// Fin del método users_find()
	
		public static function count_all() // contar total registros()
		{	// Método que retorna el número total de registros de una tabla de la base de datos
			global $db_obj;
			$query = "SELECT COUNT(*) FROM  " . static::$table_name . ";";
			$query_result = $db_obj->query_send($query);
			$register = $db_obj->fetch_array($query_result);
			// Obtener el primer valor del array que retorna find_query()
			return array_shift($register);
		} 	// Fin del método count_all()
	
		public static function find_query($query) // buscar_por_sql()
		{   // Método que retorna una matriz con los registros solicitados en el $query
			global $db_obj;
			$query_result = $db_obj->query_send($query);
			$array_objects = array();
			while ($register = $db_obj->fetch_array($query_result)) 
			{
				$class = get_called_class();
				//echo "<script language = JavaScript> alert ('find_query(query) clase que se instancia = {$class}')</script>";
				array_push($array_objects, $class::instance($register));
			}
			return $array_objects;
		} 	// Fin del método find_query()

		public static function instance($array) // instanciar()
		{	// Método para instanciar una clase
			$class_name = get_called_class(); // Clase que invoca el método de la clase tabla
			$object = new $class_name;
			
			foreach ($array as $property => $value) 
			{
				if ($object->find_property($property))
				{
					$object->$property = $value;
				}
			}
			return $object;
		} 	// Fin del método instance()

		public function find_property($property) // propiedad_existe()
		{   // Función que comprueba si la propiedad que recibe es una propiedad de la clase y Devuelve la matriz propiedad valor del objeto $this
			//$properties = $this->properties();
			$properties = get_object_vars($this);
			$array = array_key_exists($property, $properties);
			return $array;
		} 	// Fin del método find_property()

		public function properties() // propiedades()
		{	// Método que retona las propiedades o campos de una tabla
			$fields = array();
			foreach (static::$table_fields as $field)
			{
				$fields[$field] = $this->$field;
			}
			return $fields;
		}	// Fin del método propeties()

		public function save() // guardar()
		{	// Método que determina en función del ID si ejecuta el método save() o update() 
			if (!isset($this->Id)) 
			{
				if($this->create()) return true;
			}
			else
			{
				if($this->update()) return true;
			}
			return false;
		} 	// Fin del método save guardar()

		public function create()
		{	// Método para insertar un nuevo registro en una tabla 
			global $db_obj;
			$properties = $this->properties();
			if ($properties == NULL)
			{
				echo "<script language = JavaScript> alert ('Clase table, método properties(), provee al método create() un array NULL')</script>";
			}
			else
			{
				//echo print_r($properties) . "<br>";
				//echo implode(",", array_keys($properties)) . "<br>"	;
				//echo implode("','", array_values($properties)) . "<br>";
				$query = "INSERT INTO " . static::$table_name . " (";
				$query .= implode(",", array_keys($properties)); // Concatenar por comas las claves de la matriz
				$query .= ") VALUES ('";
				$query .= implode("','", array_values($properties)) . "');"; // Concatenar por comas los valores de la matriz
	 			if ($db_obj->query_send($query))
	 			{
		 			$this->Id = $db_obj->insert_id();
		 			return true;
	 			}
	 			else
	 			{
	 				return false;
	 			}
			}
		} 	// Fin del método create()

		public function update()
		{	// Método para actualizar un registro existente en una tabla
			global $db_obj;
			$properties = $this->properties();
			$formatted_properties = array();
			
			foreach ($properties as $property => $value) 
			{
				array_push($formatted_properties, "{$property}='{$value}'");
			}
			$query = "UPDATE " . static::$table_name . " SET ";
			$query .= implode(",", $formatted_properties);
			$query .= " WHERE Id = " . $db_obj->query_prepary($this->Id) . ";";
 			
 			$db_obj->query_send($query); // enviar_consulta()

 			if ($db_obj->affected_rows() == 1) 
 			{
 				$this->Id = $db_obj->insert_id();
 				return true;
 			}
 			else
 			{
 				return false;
			}
		} 	// Fin del método update()

		public function delete()
		{	// Método para eliminar un registro existente en una tabla
			global $db_obj;
			$query = "DELETE FROM " . static::$table_name;
			$query .= " WHERE Id = " . $db_obj->query_prepary($this->Id);
 			$db_obj->query_send($query); // enviar_consulta()
 			if ($db_obj->affected_rows() == 1) 
 			{
 				return true;
 			}
 			else
 			{
 				return false;
			}
		} 	// Fin del método delete()

	} 	// Fin de la clase table()
?>
