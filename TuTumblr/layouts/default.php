<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Default Layout</title>
	<script type="text/javascript" src="<?php echo WEBSITE.DS.APP_ROOT.DS ?>public/js/tinymce/tinymce.min.js"></script>
	<script>
		tinymce.init({
			selector: "textarea",
			theme: "modern",
			plugins: [
				"advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
				"searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
				"save table contextmenu directionality emoticons template paste textcolor"
			],
			content_css: "<?php echo WEBSITE.DS.APP_ROOT.DS ?>public/css/aplications.css",
			toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media fullpage | forecolor backcolor emoticons", 
			style_formats: [
				{title: 'Bold text', inline: 'b'},
				{title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
				{title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
				{title: 'Example 1', inline: 'span', classes: 'example1'},
				{title: 'Example 2', inline: 'span', classes: 'example2'},
				{title: 'Table styles'},
				{title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
		    ]
		 }); 
	</script>
</head>
<body>
	<?php if(!empty($_SESSION["message"]["success"])) { ?>
		<span class="message-success"><?php echo $_SESSION["message"]["success"]; ?></span>
	<?php } ?>
	<h2>Default Layout</h2>
	<?php include (VIEWS_PAHT.$route["controller"].DS.$route["view"].".php"); ?>
</body>
</html>