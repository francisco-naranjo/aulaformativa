 <?php 
 	class session
	{ 
		public $Id; // $Id
		public $usuario; // $usuario
		private $authenticated_user = false; // $logueado

		function __construct()
		{	// Inicio de la función construc()
			session_start();
			$this->login_verify(); // verificar_logueo()
		}	// Fin de la función __construct()

		public function login_authorized() // esta_logueado()
		{	// Función que verifica si el usuario ya inicio sesión
			// Devuelve la bandera de autenticación
			return $this->authenticated_user; // $logueado
		} 	// Fin de la función login_authorized()

		public function log_in($user_obj) // loguearse()
		{ 	// Función que permite el inicio de sesión "Loguearse"
			if ($user_obj) 
			{
				$this->Id = $_SESSION["Id"] = $user_obj->Id;  // $Id
				$this->usuario = $_SESSION["usuario"] = $user_obj->usuario;  // $usuario
				$this->authenticated_user = true; // $logueado
			} 
		} 	// Fin de la función log_in()

		public function log_out($Id) // desloguearse()
		{ // Función que finaliza la sesión sesión
			unset($this->Id);  // $Id
			unset($_SESSION["Id"]);
			$this->authenticated_user = false; // $logueado
		} // Fin de la función log_out()

		private function login_verify() // verificar_logueo()
		{ // Función que verifica si el usuario ya inicio sesión
			if (isset($_SESSION["Id"])) 
			{
				$this->Id = $_SESSION["Id"]; // $Id
				$this->authenticated_user = true; // $logueado
			} 
			else 
			{
				unset($this->Id); // $Id
				$this->authenticated_user = false; // $logueado
			}
		} // Fin de la función login_verify()

	} // Fin de la clase session

	$session = new session();
?>