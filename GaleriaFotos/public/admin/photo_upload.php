<?php 
	include("../../includes/initialize.php");
	include("../layouts/admin_header.php");
	if (!$session->login_authorized()) 
	{	// Si el usuario no está logueado se redirige a login.php
		$url = "login.php";
		url_redirect($url);
	}

	if(isset($_POST["submit"]))
	{	// Si existe en la matríz super global $_POST[], la variable submit (si se ejecuta desde el formulario)
		$photo = new photo();
		$photo->titulo = $_POST["title"];
		$photo->attach($_FILES["file_upload"]);
		if($photo->save_photo())
		{
			$message = "Archivo almacenado con éxito";
		}
		else
		{
			$message = "Existen los siguientes errores: <br>";
			$message .= join("<br>", $photo->errors);
		}
	}
?>
	<div id="body">
		<p><a href="index.php">&lt;&lt; Regresar</a></p>
		<h2>Photo UpLoad</h2>
		<?php 
			if (isset($message)) 
			{
				echo "<p> {$message} </p>";
			}
		?>
		<form action="photo_upload.php" enctype="multipart/form-data" method="post">
			<input type="hidden" name="MAX_FILE_SIZE" value="1000000" />
			<input type="file" name="file_upload" />
			<p>
				Título: <input type="text" name="title" value="" /><br>
			</p>
			<input type="submit" name="submit" value="Subir Foto" />
		</form>
		<h3><a href='logout.php'>Cerrar Sesión</a></h3>
	</div> <!-- Fin <div id="body"> -->
<?php include("../layouts/admin_footer.php"); ?>
