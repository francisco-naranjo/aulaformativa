﻿<?php require_once("../includes/initialize.php");
	include_layout("layouts/public_header.php");

	$pageindex = (isset($_GET["pageindex"]) || !empty($_GET["pageindex"])) ? (int)$_GET["pageindex"] : 1;
	$page_records = 1; // registros por página
	$total_records = photo::count_all();
	$pagination = new pagination($pageindex, $page_records, $total_records);
	$query = "SELECT * FROM fotos LIMIT {$page_records} OFFSET " . $pagination->offset();
	$photos = photo::find_query($query);
?>
	<div id="body">
		<h2>Todas las Fotos</h2>
	<?php foreach ($photos as $photo) { ?>
		<div style="display:inline-block; margin:10px;">
			<a href=<?php echo "\"full_screen.php?Id=" . $photo->Id . "\""; ?> >
				<img src=<?php echo "\"" . $photo->path_photo() . "\"";?> height="100" />
			</a>
			<p><?php echo $photo->titulo; ?></p>
		</div>
		<div style="clear:both;">
			<?php 
				//echo ($pagination->page_preview_exist()) ? '<a href="index.php?pageindex='.$pagination->page_preview().'">Anterior</a> ' : '<span style="color:grey;">Anterior</span> ' ;
				if ($pagination->page_preview_exist()) {
					echo '<a href="index.php?pageindex='.$pagination->page_preview().'">Anterior</a> ';
				}
			?>
	<div>
		<?php
			echo "Páginas: ";
			//$page_max = (($page_min) < $pagination->page_total()-3) ? $page_min+3 : $page_min;
			$pages_links = 6;
			$pages_total = $pagination->page_total();
			$page_min = 0;
			$page_max = 0;
			if (($_GET["pageindex"] - ceil($pages_links/2)) > 1) {
				if (($_GET["pageindex"] + ceil($pages_links/2)) > ($pagination->page_total())) {
					$page_max = $pagination->page_total();
					$page_min = $pagination->page_total() - $pages_links;
				}
				else {
					$page_max = $_GET["pageindex"] + 3;
					$page_min = $_GET["pageindex"] - 3;
				}
			}
			else {
				$page_min = 1;
				if (($_GET["pageindex"] + 6) > ($pagination->page_total())) {
					$page_max = $pagination->page_total();
				}
				else {
					$page_max = $page_min + 3;
				}
			}
			
			//$page_min = (($_GET["pageindex"]-3) < 1) ? 1 : ($_GET["pageindex"] - 3);
			//echo "<script language = JavaScript> alert ('page_min -> {$page_min}\\npage_max -> {$page_max}')</script>";
			for ($i=$page_min; $i <= $page_max; $i++) { 
				if ($pagination->page_index == $i) { echo '<b>'.$i.'</b> '; }
				else { echo '<a href="index.php?pageindex='.$i.'">'.$i.'</a> '; }
			}

			if ($pagination->page_preview_exist()) {
				echo '<a href="index.php?pageindex=1"><img src="icons/Pager_First.gif" /></a> ';
				echo '<a href="index.php?pageindex='.$pagination->page_preview().'"><img src="icons/Pager_Previous.gif" /></a> ';
			}
			else {
				echo '<img src="icons/Pager_First_Off.gif" /> ';
				echo '<img src="icons/Pager_Previous_Off.gif" /> ';
			}
			if ($pagination->page_next_exist()) {
				echo '<a href="index.php?pageindex='.$pagination->page_next().'"><img src="icons/Pager_Next.gif" /></a> ';
				echo '<a href="index.php?pageindex='.$pagination->page_total().'"><img src="icons/Pager_Last.gif" /></a> ';
			}
			else {
				echo '<img src="icons/Pager_Next_Off.gif" /> ';
				echo '<img src="icons/Pager_Last_Off.gif" /> ';
			}
		?>
	</div>
<?php include_layout("layouts/admin_footer.php"); ?>
