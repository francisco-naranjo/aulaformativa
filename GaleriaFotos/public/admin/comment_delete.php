﻿<?php 
	include("../../includes/initialize.php");

	if (!$session->login_authorized()) // esta_logueado()
	{	// Si el usuario no está logueado se redirige a login.php
		$url = "login.php";
		url_redirect($url); // redireccionar_a()
	}

	$message = "";
	if (!empty($_GET["Id"]))
	{
		$comment = comment::find_Id($_GET["Id"]); // buscar_por_id()
		if ($comment && $comment->delete())
		{
			$message = "Se eliminó el comentario con éxito";
		}
		else
		{
			$message = "ERROR, no se eliminó el comentario";
		}
	}

	$url = "photo_list.php?message={$message}";
	url_redirect($url);
	
	if (isset($db_obj)) $db_obj->db_connetion_close(); 
?>