 <?php require_once(LIB_DIR . SD . "database.php");

	class photo extends table
	{	// Clase photo que que se extiende de la clase base table
		public $Id;
		public $archivo;
		public $tipo;
		public $peso;
		public $titulo;

		protected static $table_name = "fotos";
		protected static $table_fields = array("archivo", "tipo", "peso", "titulo");

		private $file_name; // nombre_temporal
		private $destination_folder = "images"; // fotos_dir

		public $errors = array();
		protected $errors_upload = array(UPLOAD_ERR_OK => "No se ha producido ningún error",
							   			 UPLOAD_ERR_INI_SIZE => "El tamaño del archivo ha excedido el máximo indicado en php.ini",
										 UPLOAD_ERR_FORM_SIZE => "El tamaño del archivo ha excedido el máximo para este formulario",
										 UPLOAD_ERR_PARTIAL => "El archivo ha subido parcialmente",
										 UPLOAD_ERR_NO_FILE => "El archivo no existe",
										 UPLOAD_ERR_NO_TMP_DIR => "El directorio temporal no existe",
										 UPLOAD_ERR_CANT_WRITE => "No se puede escribir en el disco duro",
										 UPLOAD_ERR_EXTENSION => "Error en una extención php"
										);

		public function attach($array_info) {// Método que recibe un array $_FILES["file_upload"] con la información de la foto a almacenar // adjuntar_foto()
			if (!$array_info || empty($array_info) || !is_array($array_info)) {
				$message = "ERROR, el parametro no existe, está vacio o no es un array <br>";
				array_push($errors, $message);
				return false;
			}
			elseif ($array_info["error"] != 0) {
				array_push($errors, $errors_upload[$array_info["error"]]);
				return false;
			}
			else {
				$this->archivo = basename($array_info["name"]);
				$this->peso = $array_info["size"];
				$this->tipo = $array_info["type"];
				$this->file_name = $array_info["tmp_name"];
				return true;
			}
		} 	// Fin del método attach()

		public function save_photo() {// Método que determina en función del ID si ejecuta el método save() o update()  // guardar()
			if (!isset($this->Id)) { // Verificar si se va a crear el nuevo registro
				if (!empty($this->errors)) {	// Verificar si la matríz errors no se encuentra vacia
					/*echo "<script language = JavaScript> alert (' 1 Clase table \\n Método save_photo() \\n if (!empty(this->errors))')</script>";*/
					return false;
				}

				if (strlen($this->titulo) > 255) {// Verificar que la longitud del título sea permitida
					/*echo "<script language = JavaScript> alert (' 2 Clase table \\n Método save_photo() \\n if (strlen(this->titulo) > 255)')</script>";*/
					$this->errors[] = "El título superó el máximo número de caracteres permitidos (255).";
					return false;
				}

				if (empty($this->file_name)) {// Verificar que el nombre del archivo este definido
					/*echo "<script language = JavaScript> alert (' 3 Clase table \\n Método save_photo() \\n if (empty(this->file_name))')</script>";*/
					$this->errors[] = "No se tienen los datos suficientes";
					return false;
				}

				$destination_path = PATH_DIR . SD . "public" . SD . $this->destination_folder . SD . $this->archivo;
				if (file_exists($destination_path)) {// Verificar que el nombre del archivo seleccionado no exista en el directorio
					/*echo "<script language = JavaScript> alert (' 4 Clase table \\n Método save_photo() \\n if (file_exists(\$destination_path)) ')</script>";*/
					$this->errors[] = "Ya se encuentra en el directorio un archivo con el mismo nombre.";
					return false;
				}

				if(move_uploaded_file($this->file_name, $destination_path)) {
					if ($this->save()) {
						return true;
					}
					else {
						$this->errors[] = "ERROR al insertar el nuevo registro en la base de datos.";
						return false;
					}
				}
				else {
					$this->errors[] = "ERROR, no se ha podido mover el archivo a una ubicación segura.";
					return false;
				}
			}
			else {
				$this->update();
			}
		} 	// Fin del método save guardar()

		public function delete_photo() {// Método para borrar las fotos del directorio images // suprimir()
			if($this->delete()) {
				$path_file = PATH_DIR . SD . "public" . SD . $this->destination_folder . SD . $this->archivo;
				return unlink($path_file);
			}
			else {
				return false;
			}
		}	// Fin del método delete_photo()

		public function path_photo() {// Método que devuelve la ruta del archivo //ruta_imagen()
			$path = $this->destination_folder . SD . $this->archivo;
			return $path;
		}	// Fin del método path_photo() 

		public function photo_comments() {// Método que devuelve la colección de comentarios de una foto // Comentarios
			setlocale(LC_ALL, "Spanish");
			$comments = comment::photo_comment($this->Id);
			return $comments;
		}	// Fin del método photo_comments()
	}// Fin de la clase photo()
?>