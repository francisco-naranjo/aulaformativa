<?php 
	require_once("../../includes/initialize.php");
	include_layout("../layouts/admin_header.php");
	if (!$session->login_authorized()) // Si el usuario no está logueado se redirige a login.php
	{
		$url = "login.php";
		url_redirect($url);
	}

?>
	<div id="body">
		<p><a href=".."<?php echo SD . "index.php\"" ?> >&lt;&lt; Regresar</a></p>
		<h2>Public Admin Index</h2>
		<ul>
			<li><a href="photo_list.php">Ver Todas las Fotos</a></li>
			<li><a href="log_file.php">Ver el Archivo Log</a></li>
		</ul>
		<h3><a href='logout.php'>Cerrar Sesión</a></h3>

<?php include_layout("../layouts/admin_footer.php"); ?>