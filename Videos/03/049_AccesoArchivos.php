<?php 
	$archivo = "archivo.txt";
	$modo_acceso = "w";
/*
	r 	modo lectura
	w 	modo escritura desde el inicio del archivo, si no existe se crea
	a 	modo agregar
	x 	modo crear el archivo, es creado por que no existe
	r+ 	leer y escribir
	w+  leer y escribir desde el principio
	a+ 	leer y escribir desde el final
	x+ 	crear leer y escribir
	wt 	crear nueva línea en unix, linux o mac \n  en windos \r\n
*/
	$archivo_abierto = fopen($archivo, $modo_acceso);
	if ($archivo_abierto)
	{
		fclose($archivo_abierto);
	}

?>