﻿<?php 
	require_once("../../includes/initialize.php");
	include_layout("../layouts/admin_header.php");

	if (!empty($_GET["Id"])) {
		$id_get = $_GET["Id"];
		//echo "<script language = JavaScript> alert ('por get ={$id_get}')</script>";
		$photo = photo::find_Id($_GET["Id"]);
		if (!$photo) {
			url_redirect("index.php");
		}
	}
	else {
		url_redirect("index.php");
	}

	//$comentarios = $photo->photo_comments();
	$comentarios = comment::photo_comment($photo->Id);
?>
<div id="body">
	<h2>Comentarios para <?php echo $photo->archivo; ?></h2>
	<pre><?php //echo print_r($comentarios); ?></pre>
	<p><a href="photo_list.php">&lt;&lt; Regresar</a></p>

	<?php foreach ($comentarios as $comentario) 
		{
	?>
			<div style="margin:15px;">
				<div><?php echo $comentario->autor; ?></div>
				<div><?php echo $comentario->contenido; ?></div>
				<div><?php echo format_date($comentario->creado); ?></div>
				<div><a href="comment_delete.php?Id=<?php echo $comentario->Id; ?>">Eliminar Comentario</a></div>
			</div>
	<?php 
		} 
	?>
<?php include_layout("../layouts/admin_footer.php"); ?>
