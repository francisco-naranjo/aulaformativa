﻿<?php 
	require_once("../includes/initialize.php");
	include_layout("layouts/public_header.php");

	if (!empty($_GET["Id"])) {
		$id_get = $_GET["Id"];
		/*echo "<script language = JavaScript> alert ('\$_GET[\"Id\"] ={$id_get}')</script>";*/
		$photo = photo::find_Id($_GET["Id"]);
		if (!$photo) {
			url_redirect("index.php");
		}
	}
	else {
		url_redirect("index.php");
	}

	$message = "";

	if (isset($_POST["submit"])) {
		$autor = trim($_POST["autor"]);
		$contenido = trim($_POST["contenido"]);

		$comentario = comment::create_comment($photo->Id, $autor, $contenido);

		if ($comentario && $comentario->save()) {
			$url = "full_screen.php?Id=" . $photo->Id;
			url_redirect($url);
		} 
		else {
			$message = "No se ha podido guardar el comentario";
		}
	}
	else {
		$autor = "";
		$contenido = "";
	}

	//$comentarios = $photo->photo_comments();
	$comentarios = comment::photo_comment($photo->Id);
?>
<div id="body">
	<pre><?php //echo print_r($comentarios); ?></pre>
	<p><a href="index.php">&lt;&lt; Regresar</a></p>
	<p><?php echo $message; ?></p>
	<h2><?php echo $photo->titulo; ?></h2>
	<div>
		<img src=<?php echo "\"" . $photo->path_photo() . "\""; ?>>
	</div>
	<?php foreach ($comentarios as $comentario) 
		{
	?>
			<div style="margin:15px;">
				<div><?php echo $comentario->autor; ?></div>
				<div><?php echo $comentario->contenido; ?></div>
				<div><?php echo format_date($comentario->creado); ?></div>
			</div>
	<?php 
		} 
	?>
	<form action=<?php echo "full_screen.php?Id=" . $photo->Id; ?> method="post">
		<table>
			<tr>
				<td>Nombre:</td>
				<td><input type="text" name="autor" value=<?php echo "\"" . $autor . "\""; ?> /></td>
			</tr>
			<tr>
				<td style="vertical-align:top;">Comentario:</td>
				<td><textarea name="contenido" cols="40" rows="8"><?php echo $contenido; ?></textarea></td>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td style="text-align:right;"><input type="submit" name="submit" value="Enviar Comentario" /></td>
			</tr>
		</table>
	</form>
<?php include_layout("layouts/admin_footer.php"); ?>
