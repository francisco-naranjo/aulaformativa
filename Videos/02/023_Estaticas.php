<?php 

class clsPerro {
	public $nombre;
	protected $raza;
	protected $color;

	//Variable estatica de la clase
	public static $totalPerros = 0;

	// Metodo estatico
	public static function funAumentarperros() {
		clsPerro::$totalPerros++;
	}

	public function funSetRaza($valor) {
		$this->raza = $valor;
	}

	public function funSetColor($valor) {
		$this->color = $valor;
	}

	public function funGetRaza() {
		return $this->raza;
	}

	public function funGetColor() {
		return $this->color;
	}
}

class clsDoberman extends clsPerro {
	protected $raza = "Doberman";
	protected $color = "Negro";

	public function funSetRaza($valor) {
	}

	public function funSetColor($valor) {
	}
}

$perro1 = new clsDoberman();
clsPerro::funAumentarperros();
echo clsPerro::$totalPerros++;
/*
$perro1->funSetColor("Amarillo");
echo $perro1->funGetColor();
*/
echo "<br>";

$perro1 = new clsPerro();
clsPerro::funAumentarperros();
echo clsPerro::$totalPerros++;
/*
$perro1->funSetColor("Amarillo");
echo $perro1->funGetColor();
*/
?>