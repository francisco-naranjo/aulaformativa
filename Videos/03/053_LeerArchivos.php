<?php 
	$archivo = "archivo.txt";
	$modo_acceso = "r";
/*
	r 	modo lectura
	w 	modo escritura desde el inicio del archivo, si no existe se crea
	a 	modo agregar
	x 	modo crear el archivo, es creado por que no existe
	r+ 	leer y escribir
	w+  leer y escribir desde el principio, sobreescribir
	a+ 	leer y escribir desde el final
	x+ 	crear leer y escribir
	wt 	crear nueva línea en unix, linux o mac \n  en windos \r\n
*/

	$archivo_abierto = fopen($archivo, $modo_acceso);
	$contenido_archivo = "";
	if ($archivo_abierto)
	{
		//$contenido_archivo = fread($archivo_abierto, 20);
		//$contenido_archivo = fread($archivo_abierto, filesize($archivo));
		//echo "<br><br>";
		//$contenido_archivo = fgets($archivo_abierto, filesize($archivo));
	 	//echo nl2br($contenido_archivo) . "<br><br>";
	 	// Mientras no se llegue al final del archivo
	 	while (!feof($archivo_abierto)) 
	 	{
	 		$contenido_archivo .= fgets($archivo_abierto);
	 	}
		fclose($archivo_abierto);
	}
	// Covertir los caracteres de nueva línea a etiquetas <br>
	echo nl2br($contenido_archivo) . "<br><br>";

 	// Devuelve el contenido total del archivo
 	$contenido_archivo = file_get_contents($archivo);
 	echo nl2br($contenido_archivo) . "<br><br>";
?>