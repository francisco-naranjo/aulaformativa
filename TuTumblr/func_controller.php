<?php 
	function redirect_to($url) {
		header("location:".WEBSITE.APP_ROOT."/".$url);
		exit();
	}

	function message_warnings($message) {
		if (empty($message)) {
			return false;
		}
		$_SESSION["message"]["warnings"] = $message;
		return true;
	}

	function message_success($message) {
		if (empty($message)) {
			return false;
		}
		$_SESSION["message"]["success"] = $message;
		return true;
	}
?>