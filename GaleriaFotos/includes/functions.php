<?php 
	// Función que recibe una url y redirige a ella
	function url_redirect($url) 
	{	// Función que recibe una url y redirecciona a esta
		header("Location: {$url}");
		exit();
	}	// Fin de la fución url_redirect()

	function __autoload($class_name)
	{	// Función que informa que si la clase no se encuentra
		// $class_name = strtolower($class_name);
		//require_once("../includes/{$class_name}.php");
		die("La clase: {$class_name} no ha sido encontrada <br>");
	}	// Fin de la fución __autoload()

	function include_layout($layout)
	{	// Función que recibe la url e incluye una plantilla
		// Incluye el archivo
		include($layout);
	}	// Fin de la fución include_layout()

	function record_actions($action, $message) // grabar_acciones()
	{	// Guardar la información en el archivo
		$path_file = PATH_DIR . SD . "logs" . SD . "log.txt";
		/*echo "<script language = JavaScript> alert ('path_file = {$path_file}')</script>";*/

		if($file = fopen($path_file, "a")) //Abro el archivo en modo adjuntar
		{
			$time = strftime("%Y-%m-%d %H:%M:%S", time());
			$string = $time . " | " . $action . " | " . $message . "\n";
			fwrite($file, $string);
			fclose($file);
		}
		else
		{
			echo "Error al intentar grabar en el archivo -> {$file}";
		} 	// Fin de la función record_actions() graba_acciones()
	}	// Fin de la fución record_actions()

	function format_date($date)
	{
		setlocale(LC_ALL, "Spanish");
		$time = strtotime($date);
		$formated_date = strftime("%d de %B del %Y a las %I:%M:%S %p", $time);
		return $formated_date;
	}
?>