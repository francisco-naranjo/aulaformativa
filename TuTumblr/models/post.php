<?php 
	/* CREA UNA NUEVA ENTRADA EN LA BD
	** @param array $params
	** @return (boolean)
	*/
	function create_post($params) {
		$connect_db_open = connect_db_open();
		$CreateDate = format_date();
		/* Insertar nuevos registros */
		$query = "INSERT INTO posts (Title, Body, CreateDate, UserId) VALUES ('" . mysql_real_escape_string($params["Title"]) . "', '" . mysql_real_escape_string($params["Body"]). "', '" . $CreateDate . "', " . mysql_real_escape_string($params["UserId"]). ");";
		echo $query . "<br>";
		/* mysql_real_escape_string() quita los caracteres no permitidos por mysql*/
		$result = mysql_query($query);
		if (!$result) {
			//die ("Se a presentado el siguiente ERROR al agregar el registro <br>" . mysql_error() . "<br>");
			return false;
		}
		else {
			return true;
		}
	}

	/* ACTUALIZA UNA NUEVA ENTRADA EN LA BD
	** @param array $params
	** @return (boolean)
	*/
	function update_post($params) {
		$connect_db_open = connect_db_open();
		/* Actualizar registros */
		$query = "UPDATE posts SET Title='" . mysql_real_escape_string($params["Title"]) . "', Body='" . mysql_real_escape_string($params["Body"]) . "', UserId='" . mysql_real_escape_string($params["UserId"]) . "' WHERE Id='" . mysql_real_escape_string($params["Id"]) . "';";
		//echo $query;
		$result = mysql_query($query);
		//if (!$result) {
		if (mysql_affected_rows() != 1) { // Si no se cambia la info almacenada, devuelve error
			//die ("Se a presentado el siguiente ERROR al actualizar el registro <br>" . mysql_error() . "<br>");
			return false;
		}
		else {
			//echo "Registro ACTUALIZADO co exito";
			return true;
		}
	}

	/* ELIMINA UNA NUEVA ENTRADA EN LA BD
	** @param int $Id
	** @return (boolean)
	*/
	function delete_post($Id) {
		$connect_db_open = connect_db_open();
		/* Eliminar registros */
		$query = 'DELETE FROM posts WHERE Id=' . mysql_real_escape_string($Id) . ';';
		//echo $query;
		mysql_query($query);
		//if (!$result) {
		if (mysql_affected_rows() != 1) { // Si no se cambia la info almacenada, devuelve error
			//die ("Se a presentado el siguiente ERROR al eliminar el registro <br>" . mysql_error() . "<br>");
			return false;
		}
		else {
			//echo "Registro ELIMINADO co exito";
			return true;
		}
	}

	/* DEVUELVE TODAS LAS ENTRADAS DE LA BD
	** @param int $Id
	** @return (boolean)
	*/
	function retrieve_posts() {
		$connect_db_open = connect_db_open();
		$query = "SELECT pos.Id, pos.Title, pos.Body, usr.UserName FROM posts AS pos, users AS usr WHERE usr.Id=pos.UserId ORDER BY pos.Id DESC;";
		$result = mysql_query($query);

		if (!$result && mysql_num_rows($result) != 0) {
			return false;
		}
		else {
			return resource_array($result);
		}
	}

	/* DEVUELVE UNA ENTRADA DE LA BD
	** @param int $Id
	** @return (array) OR (boolean)
	*/
	function retrieve_post($Id) {
		$connect_db_open = connect_db_open();
		$query = "SELECT pos.Id, pos.Title, pos.Body, usr.UserName FROM posts AS pos, users AS usr WHERE usr.Id=pos.UserId AND pos.Id={$Id};";
		$result = mysql_query($query);
		//echo $query;
		if (!$result) {
			return false;
		}
		else {
			return mysql_fetch_array($result);
		}
	}

	/* FORMATEA LA FECHA A YYYY-MM-DD HH:MM:SS
	** @return string $formated_date
	*/
	function format_date() {
		$formated_date = strftime("%Y-%m-%d %H:%M:%S", time());
		return $formated_date;
	}
?>