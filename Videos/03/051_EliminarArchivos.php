<?php 
	$archivo = "archivo2.txt";
	$modo_acceso = "w";
/*
	r 	modo lectura
	w 	modo escritura desde el inicio del archivo, si no existe se crea
	a 	modo agregar
	x 	modo crear el archivo, es creado por que no existe
	r+ 	leer y escribir
	w+  leer y escribir desde el principio, sobreescribir
	a+ 	leer y escribir desde el final
	x+ 	crear leer y escribir
	wt 	crear nueva línea en unix, linux o mac \n  en windos \r\n
*/
	unlink($archivo); //Se debe tener permisos de escritura en la carpeta
	echo "Archivo: {$archivo} eliminado exitosamente";
?>