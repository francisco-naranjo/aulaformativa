<?php 
	if (isset($_POST{"submit"})) 
	// CÓDIGOS DE ERRORES
	// UPLOAD_ERR_OK => 0
	// UPLOAD_ERR_INI_SIZE => 1  TAMAÑO MAXIMO NO PERMITIDO
	// UPLOAD_ERR_FORM_SIZE => 2
	// UPLOAD_ERR_PARTIAL => 3  ARCHIVO SUBIDO PARCIALMENTE
	// UPLOAD_ERR_NO_FILE => 4   NO EXISTE EL ARCHIVO
	// UPLOAD_ERR_NO_TMP_DIR => 6   NO EXISTE EL DIRECTORIO TEMPORAL
	// UPLOAD_ERR_CANT_WRITE => 7   NO TIENE PERMISOS DE ESCRITURA
	// UPLOAD_ERR_EXTENSION => 8   NO ESTA HABILITADA LA EXTENCIONES DE PHP
	{	
		$errors = array(UPLOAD_ERR_OK => "No se ha producido ningún error",
						UPLOAD_ERR_INI_SIZE => "El tamaño del archivo ha excedido el máximo indicado en php.ini",
						UPLOAD_ERR_FORM_SIZE => "El tamaño del archivo ha excedido el máximo para este formulario",
						UPLOAD_ERR_PARTIAL => "El archivo ha subido parcialmente",
						UPLOAD_ERR_NO_FILE => "El archivo no existe",
						UPLOAD_ERR_NO_TMP_DIR => "El directorio temporal no existe",
						UPLOAD_ERR_CANT_WRITE => "No se puede escribir en el disco duro",
						UPLOAD_ERR_EXTENSION => "Error en una extención php"
					   );

		defined("SD") ? NULL : define("SD",DIRECTORY_SEPARATOR);
		defined("PATH_DIR") ? NULL : define("PATH_DIR",$_SERVER["DOCUMENT_ROOT"] . SD . "uploads");
		
		$file_temporal = $_FILES["file_upload"]["tmp_name"];
		//echo "<script language = JavaScript> alert ('file_temporal = {$file_temporal}')</script>";
		$file_name = $_FILES["file_upload"]["name"];
		//echo "<script language = JavaScript> alert ('file_name = {$file_name}')</script>";
			$file_destination = "../../uploads/".basename($file_name);
		//$file_destination = PATH_DIR . SD . basename($file_name);
		//echo "<script language = JavaScript> alert ('file_destination = {$file_destination}')</script>";

		if (move_uploaded_file($file_temporal, $file_destination)) 
		{
			$message = "Archivo cargado con éxito";
		}
		else
		{
			$message = $errors[$_FILES["file_upload"]["error"]];
		}
	}
?>
<!DOCTYPE html>
<html lang="es">
<head>
        <meta charset="UTF-8">
        <link type="text/css" rel="stylesheet" href="../style/main.css" />
        <title>063_MoverArchivosSubidos.php</title>
</head>
<body>
	<?php if (isset($message)) { echo "<p>" . $message . "</p>"; } ?>
	<form id="upload_file_form" method="post" enctype="multipart/form-data" action="063_MoverArchivosSubidos.php">
		<input type="hidden" name="MAX_FILE_SIZE" id="MAX_FILE_SIZE" value="1000000"/><br>
		<input type="file" name="file_upload" id="file_upload" value=""/><br>
		<input type="submit" name="submit" id="submit" value="Guardar Archivo"/>
	</form>
	<p>
		<a href="../../uploads">uploads</a>
	</p>
</body>
</html>