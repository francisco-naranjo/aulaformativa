<?php 
	include("../../includes/initialize.php");
	include("../layouts/admin_header.php");
	if (!$session->login_authorized()) // Si el usuario no está logueado se redirige a login.php
	{
		$url = "login.php";
		url_redirect($url);
	}

	if (isset($_GET["clear"]) && $_GET["clear"] == 1) 
	{
		$path_file = PATH_DIR . SD . "logs" . SD . "log.txt";
		file_put_contents($path_file, "");
		$action = "Limpiar"; 
		$message = "El Usuario " . $_SESSION["usuario"] . " con Id " . $session->Id . " ha limpiado el Archivo log.txt";
		record_actions($action, $message);
		url_redirect("log_file.php"); // redireccionar_a()
	}
?>
	<div id="body">
		<h2>Public Admin log_file</h2>
		<p><a href=<?php echo "\"index.php\"" ?> >&lt;&lt; Regresar</a></p>
		<ul>
			<?php 
				$path_file = PATH_DIR . SD . "logs" . SD . "log.txt"; 
				if (file_exists($path_file) && is_readable($path_file) && $file = fopen($path_file, "r")) 
				{
					while (!feof($file)) 
					{
						$content_file = trim(fgets($file));
						if ($content_file != "") 
						{
							echo "<li>{$content_file}</li>";
						}
					}
					fclose($file);
				}
			?>
		</ul>
		<?php echo "<h3><a href='logout.php'>Cerrar Sesión</a></h3>"; ?>
		<p><a href="log_file.php?clear=1">Limpiar Contenido Archivo Log</a></p>
<?php include("../layouts/admin_footer.php"); ?>
