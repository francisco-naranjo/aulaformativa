<?php 
	//En sistema octal
	//El primer de los cuatro números siempre es un cero
	//El segundo es la suma de los permisos del usuario propitario del archivo
	//El tercero es la suma de los permisos de los usuario propitario del archivo
	chmod("archivo.txt", 0644);
	echo substr(decoct(fileperms("archivo.txt")),2)."<br>"; 
	echo is_readable("archivo.txt") ? "CON PERMISO DE LECTURA <br>" : "SIN PERMISO DE LECTURA <br>";
	echo is_writable("archivo.txt") ? "CON PERMISO DE ESCRITURA <br>" : "SIN PERMISO DE ESCRITURA <br>";
?>