<?php 
	// MATRIZ QUE CONTIENE LAS EXPRESIONES REGULARES CON SUS CONTROLADORES Y VISTAS
	$routes = array(array('url' => '/^posts\/(?P<Id>\d+)\/?$/', 
						 "controller"=>"posts", 
						 "view"=>"show"), 
				    array('url' => '/^posts\/create\/?$/', 
				    	  "controller"=>"posts", 
				    	  "view"=>"create"),
				    array('url' => '/^posts\/new\/?$/', 
				    	  "controller"=>"posts", 
				    	  "view"=>"new"),
				    array('url' => '/^posts\/(?P<Id>\d+)\/edit\/?$/', 
				    	  "controller"=>"posts", 
				    	  "view"=>"edit"),
				    array('url' => '/^posts\/?$/', 
				    	  "controller"=>"posts", 
				    	  "view"=>"index"),
				    array('url' => '/^posts\/(?P<Id>\d+)\/update\/?$/', 
				    	  "controller"=>"posts", 
				    	  "view"=>"update"),
				    array('url' => '/^posts\/(?P<Id>\d+)\/delete\/?$/', 
				    	  "controller"=>"posts", 
				    	  "view"=>"delete")
			);

	// CONSTANTES DEFINIDAS PARA LA BASE DE DATOS
	define("SERVER_RUN", $_SERVER['SERVER_NAME']);

	if (SERVER_RUN == "localhost") {
		define("DB_SERVER","localhost");
		define("DB_NAME","tumblr");
		define("DB_USERNAME","test");
		define("DB_PASSWORD","test");
	}
	else {
		define("DB_SERVER","localhost");
		define("DB_NAME","tumblr");
		define("DB_USERNAME","test");
		define("DB_PASSWORD","test");
	}

	// CARPETA RAIZ DEL PROYECTO
	define("APP_ROOT","AulaFormativa/TuTumblr");

	// CARPETA RAIZ DEL PROYECTO
	define("WEBSITE","http://localhost:81/");

	// SEPARADOR DE DIRECTORIO
	define("DS", "/");

	// $_SERVER["DOCUMENT_ROOT"]
	define("SERVER_ROOT", $_SERVER["DOCUMENT_ROOT"]);

	// RUTA DE CONTROLADORES
	define("CONTROLLERS_PAHT", SERVER_ROOT.DS.APP_ROOT.DS."controllers".DS);

	// RUTA DE MODELOS
	define("MODELS_PAHT", SERVER_ROOT.DS.APP_ROOT.DS."models".DS);

	// RUTA DE VISTA
	define("VIEWS_PAHT", SERVER_ROOT.DS.APP_ROOT.DS."views".DS);

	// RUTA DE LAYOUTS
	define("LAYOUTS_PAHT", SERVER_ROOT.DS.APP_ROOT.DS."layouts".DS);

	// LIBRERIA DE INCLUDES
	require("database.php");
	require("func_controller.php");
?>