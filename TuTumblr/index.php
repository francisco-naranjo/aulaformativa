<?php session_start();
	require("lib/config.php");
	function routing($routes) {
		// OBTENER LA URL DE LA BARRA DE DIRECCIONES
		$url = $_SERVER["REQUEST_URI"];

		// RECORTAR LA URL DEJANDO LO NECESARIO PARA REALIZAR EL MAPEO
		$url = str_replace("/".APP_ROOT."/", "", $url);
		// CONCATENAR LOS PARAMETROS RECIBIDOS POR GET
		$url = str_replace("?".$_SERVER["QUERY_STRING"], "", $url);

		$params = params();

		// BUSCAR COINCIDENCIAS DE URL A TAVES DE LAS DIFERENTES EXPRESIONES REGULARES
		foreach ($routes as $route) {
			//echo "Realizando comprobacion <br>";
			if ($n_routes = preg_match($route["url"], $url, $matches) > 0) {
				//echo 'Coincidencia encontrada <br>';
				$params = array_merge($matches, $params);
				break;
			}
		}
		// COMPROBAR SI EXISTEN COINCIDENCIAS
		if ($n_routes == 0) {
			exit("No se ha encontrado la ruta");
		}
		//echo "<pre>"; print_r($params); echo "</pre>";
		//echo $url;

		// INCLUIR EL ARCHIVO PHP DE ACUERDO AL MAPEO REALIZADO
		include (CONTROLLERS_PAHT . $route["controller"] . ".php");

		//SI NO EXISTE EL LAYOUT, CARGAR UNO POR DEFECTO
		if (file_exists(LAYOUTS_PAHT . $route["controller"] . ".php")) {
			include (LAYOUTS_PAHT . $route["controller"] . ".php");
		}
		else {
			include (LAYOUTS_PAHT . "default.php");
		}

		// LIMPIAR EL $_SESSION["message"]["success"])
		$_SESSION["message"]["success"] = "";
		$_SESSION["message"]["warnings"] = "";
	}

	/* CREAR MATRIZ PARAMS VERIFICANDO CONTENIDO POST Y GET
	** @return (array) $params
	*/
	function params() {
		$params = array();
		if (!empty($_POST)) {
			$params = array_merge($params, $_POST);
		}
		if (!empty($_GET)) {
			$params = array_merge($params, $_GET);
		}
		return $params;
	}

	routing($routes);
?>