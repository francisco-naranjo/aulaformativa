<?php 
	define("SERVER_RUN", $_SERVER['SERVER_NAME']);  // Servidor Remoto = remoto --- Servidor Local = local

	if (SERVER_RUN == "localhost") // Servidor Local
	{
		defined("SD") ? NULL : define("SD", "/");
		defined("PATH_DIR") ? NULL : define("PATH_DIR", $_SERVER["DOCUMENT_ROOT"] . SD . "AulaFormativa" . SD . "GaleriaFotos");
	}
	else // Servidor remoto o de producción 
	{
		defined("SD") ? NULL : define("SD", DIRECTORY_SEPARATOR);
		defined("PATH_DIR") ? NULL : define("PATH_DIR", $_SERVER["DOCUMENT_ROOT"] . SD . "GaleriaFotos");
	}
	
	defined("LIB_DIR") ? NULL : define("LIB_DIR", PATH_DIR . SD . "includes");	

	//echo (LIB_DIR . SD . "config.php"); 
	require_once(LIB_DIR . SD . "config.php");
	require_once(LIB_DIR . SD . "database.php");
	require_once(LIB_DIR . SD . "table.php");
	require_once(LIB_DIR . SD . "functions.php");
	require_once(LIB_DIR . SD . "pagination.php");
	require_once(LIB_DIR . SD . "user.php");
	require_once(LIB_DIR . SD . "photo.php");
	require_once(LIB_DIR . SD . "comment.php");
	require_once(LIB_DIR . SD . "session.php");
?>