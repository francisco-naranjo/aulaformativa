﻿<?php 
	include("../../includes/initialize.php");

	if (!$session->login_authorized()) // esta_logueado()
	{	// Si el usuario no está logueado se redirige a login.php
		$url = "login.php";
		url_redirect($url); // redireccionar_a()
	}

	$message = "";
	if (!empty($_GET["Id"]))
	{
		$photo = photo::find_Id($_GET["Id"]); // buscar_por_id()
		if ($photo->delete_photo())
		{
			$message = "Se eliminó la foto con éxito";
		}
		else
		{
			$message = "SERROR, no se eliminó la foto";
		}
	}

	$url = "photo_list.php?message={$message}";
	url_redirect($url);
	
	if (isset($db_obj)) $db_obj->db_connetion_close(); 
?>