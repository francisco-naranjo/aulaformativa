<?php 

class clsPerro {
	public $nombre;
	protected $raza;
	protected $color;

	//Variable estatica de la clase
	public static $totalPerros = 0;

	// Metodo estatico
	public static function funAumentarperros() {
		// :: es un operador de resolucion de ambito
		//clsPerro::$totalPerros++;
		self::$totalPerros++; // Solo funciona dentro del ambito de una clase
	}

	public function funSetRaza($valor) {
		$this->raza = $valor;
	}

	public function funSetColor($valor) {
		$this->color = $valor;
	}

	public function funGetRaza() {
		return $this->raza;
	}

	public function funGetColor() {
		return $this->color;
	}
}

class clsDoberman extends clsPerro {
	protected $raza = "Doberman";
	protected $color = "Negro";

	public function funSetRaza($valor) {
	}

	public function funSetColor($valor) {
		echo "Generalmente el color de la raza Doberman es negro... <br>";
		// Llamamos el metodo de la clase padre
		parent::funSetColor($valor);
	}

	public function funCantidadPerros() {
		return parent::$totalPerros;
	}
}

$perro1 = new clsDoberman();
$perro1->funSetColor("Amarillo");
echo $perro1->funGetColor();
?>