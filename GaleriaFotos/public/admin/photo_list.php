﻿<?php include("../../includes/initialize.php");
	include("../layouts/admin_header.php");
	if (!$session->login_authorized()) {// Si el usuario no está logueado se redirige a login.php // esta_logueado()
		$url = "login.php";
		url_redirect($url); // redireccionar_a()
	}

	$photos = photo::find_all(); // buscar_todos()
?>
	<div id="body">
		<h2>Todas las Fotos</h2>
		<p><a href="index.php">&lt;&lt; Regresar</a></p>
		<?php 
			//$message = $_GET["message"];
			if (!empty($_GET["message"])) {	
				$message = $_GET["message"];
				echo "<p>{$message}</p>";
			}
		?>
		<table border="1">
			<tr>
				<th>Imagen</th>
				<th>Nombre Archivo</th>
				<th>Título</th>
				<th>Tamaño</th>
				<th>Tipo</th>
				<th>Comentarios</th>
				<th>Opciones</th>
			</tr>
			<?php
				foreach ($photos as $photo) {
			?>
				<tr>
					<td><img width="100" src="..<?php echo SD . $photo->path_photo(); ?>" /></td>
					<td><?php echo $photo->archivo; ?></td>
					<td><?php echo $photo->titulo; ?></td>
					<td><?php echo round(((float)$photo->peso)/1024) . " Kb"; ?></td>
					<td><?php echo $photo->archivo; ?></td>
					<td style="text-align: center;"><a href="comment_list.php?Id=<?php echo $photo->Id; ?>"><?php echo count($photo->photo_comments()); ?></a></td>
					<td style="text-align: center;"><a href="photo_delete.php?Id=<?php echo $photo->Id; ?> ">Eliminar</a></td>
				</tr>
			<?php 
				} 
			?>
		</table>
		<p><a href="photo_upload.php">&gt;&gt; Agregar Foto</p>
		<h3><a href='logout.php'>Cerrar Sesión</a></h3>
	</div> <!-- Fin <div id="body"> -->
<?php include("../layouts/admin_footer.php"); ?>