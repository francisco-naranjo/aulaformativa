<?php 

class clsPerro {
	public $nombre;
	protected $raza;
	protected $color;	

	public function funSetRaza($valor) {
		$this->raza = $valor;
	}

	public function funSetColor($valor) {
		$this->color = $valor;
	}

	public function funGetRaza() {
		return $this->raza;
	}

	public function funGetColor() {
		return $this->color;
	}
}

class clsDoberman extends clsPerro {
	protected $raza = "Doberman";
	protected $color = "Negro";

	public function funSetRaza($valor) {
	}

	public function funSetColor($valor) {
	}
}

$perro1 = new clsDoberman();
$perro1->funSetColor("Amarillo");
echo $perro1->funGetColor();

echo "<br>";

$perro1 = new clsPerro();
$perro1->funSetColor("Amarillo");
echo $perro1->funGetColor();

?>