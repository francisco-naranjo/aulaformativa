<?php 

class clsPerro {
	public $nombre;
	protected $raza;
	protected $color;

	//Variable estatica de la clase, PROPIEDAD Privada
	private static $totalPerros = 0;

	function __construct() {
		clsPerro::$totalPerros++;
	}

	public static function funGetTotalPerros() {
		return self::$totalPerros;
	}

	public function funSetRaza($valor) {
		$this->raza = $valor;
	}

	public function funSetColor($valor) {
		$this->color = $valor;
	}

	public function funGetRaza() {
		return $this->raza;
	}

	public function funGetColor() {
		return $this->color;
	}
}

class clsDoberman extends clsPerro {
	
	function __construct() {
		$raza = "Doberman";
		$color = "Negro";
		//clsPerro::$totalPerros++;
		//clsDoberman::$totalPerros++;
		//self::$totalPerros++;
		//parent::$totalPerros++;
		parent::__construct(); // Solo si es privada en clsPerro
	}

	public function funSetRaza($valor) {
	}

	public function funSetColor($valor) {
		echo "Generalmente el color de la raza Doberman es negro... <br>";
		// Llamamos el metodo de la clase padre
		parent::funSetColor($valor);
	}

}

$perro1 = new clsPerro();
$perro2 = new clsPerro();
$perro3 = new clsDoberman();
//$perro1->funSetColor("Amarillo");
echo clsPerro::funGetTotalPerros();

?>