<?php 
	$archivo = "archivo.txt";
	$modo_acceso = "w+";
/*
	r 	modo lectura
	w 	modo escritura desde el inicio del archivo, si no existe se crea
	a 	modo agregar
	x 	modo crear el archivo, es creado por que no existe
	r+ 	leer y escribir			
	w+  leer y escribir desde el principio, sobreescribir
	a+ 	leer y escribir desde el final
	x+ 	crear leer y escribir
	wt 	crear nueva línea en unix, linux o mac \n  en windos \r\n
*/
	$archivo_abierto = fopen($archivo, $modo_acceso);
	if ($archivo_abierto)
	{
		$texto_incluido = "MacBookaro \r\nMacBookaro \r\n";
		fwrite($archivo_abierto, $texto_incluido);
		fwrite($archivo_abierto, $texto_incluido);
		fclose($archivo_abierto);
	}

	// Sobreescribir el contenido del archivo
	$contenido = "Estudiante \r\nUMB \r\n";
	$kb = file_put_contents($archivo, $contenido);
	echo "KB: " . $kb;
?>